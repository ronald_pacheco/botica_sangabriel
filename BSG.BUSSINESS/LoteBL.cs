﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSG.DATAACCESS;
using BSG.DATACONTRACT;

namespace BSG.BUSSINESS
{
    public class LoteBL
    {
        #region Singleton

        static LoteBL() { }
        private LoteBL() { }
        private static LoteBL _instancia = new LoteBL();
        public static LoteBL Instancia { get { return _instancia; } }

        #endregion

        public LoteBE RegistrarLote(LoteBE NuevoLote, ref bool result, ref string mensaje)
        {
            return LoteAccess.Instancia.RegistrarLote(NuevoLote, ref result, ref mensaje);
        }

        public List<LoteBE> ObtenerLotes(LoteBE Lote, ref bool result, ref string mensaje)
        {
            return LoteAccess.Instancia.ObtenerLotes(Lote, ref result, ref mensaje);
        }

        public LoteBE EditarLote(LoteBE EditarLote, ref bool result, ref string mensaje)
        {
            return LoteAccess.Instancia.EditarLote(EditarLote, ref result, ref mensaje);
        }

        public LoteBE EliminarLote(LoteBE EliminarLote, ref bool result, ref string mensaje)
        {
            return LoteAccess.Instancia.EliminarLote(EliminarLote, ref result, ref mensaje);
        }
    }
}
