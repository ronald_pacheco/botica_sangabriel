﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSG.DATAACCESS;
using BSG.DATACONTRACT;
using System.Transactions;

namespace BSG.BUSSINESS
{
    public class VentaBL
    {
        #region Singleton

        static VentaBL() { }
        private VentaBL() { }
        private static VentaBL _instancia = new VentaBL();
        public static VentaBL Instancia { get { return _instancia; } }

        #endregion

        public void GenerarVenta(VentaBE VentaGenerar, ref bool result, ref string mensaje)
        {
            using (var scope = new TransactionScope())
            {
                VentaBE NuevaVenta = new VentaBE();
                NuevaVenta.importe = VentaGenerar.ListaPreVenta.Sum(x => x.SubTotal);
                NuevaVenta.cliente = VentaGenerar.cliente;
                NuevaVenta.UsuarioMaster = VentaGenerar.UsuarioMaster;
                NuevaVenta.idVenta = VentaAccess.Instancia.RegistrarVenta(NuevaVenta, ref result, ref mensaje);
                if (result)
                {
                    List<VentaDetalleBE> ListaVentaDetalle = new List<VentaDetalleBE>();
                    foreach (var VntDetalle in VentaGenerar.ListaPreVenta)
                    {
                        VentaDetalleBE VentaDetalle = new VentaDetalleBE();
                        VentaDetalle.idVenta = NuevaVenta.idVenta;
                        VentaDetalle.idProducto = VntDetalle.idProducto;
                        VentaDetalle.cantidad = VntDetalle.Cantidadllevar;
                        VentaDetalle.importe = VntDetalle.SubTotal;
                        LoteBE LoteBuscar = new LoteBE();
                        LoteBuscar.idProducto = VntDetalle.idProducto;
                        LoteBuscar.EstadoLote = "8";
                        List<LoteBE> ListaLotes = LoteBL.Instancia.ObtenerLotes(LoteBuscar, ref result, ref mensaje).Where(x => x.DiferenciaDias > 0).OrderBy(x => x.fechaVencimiento).ToList();
                        int CantidadReal = VentaDetalle.cantidad;
                        foreach (var itemLote in ListaLotes)
                        {
                            if (result)
                            {
                                LoteBE StockActualizar = new LoteBE();
                                VentaDetalle.idLote = itemLote.idLote;
                                StockActualizar.idLote = itemLote.idLote;
                                if (CantidadReal <= itemLote.StockDisponible)
                                {
                                    VentaDetalle.cantidad = CantidadReal;
                                    StockActualizar.StockDisponible = CantidadReal;
                                    VentaAccess.Instancia.RegistrarVentaDetalle(VentaDetalle, ref result, ref mensaje);
                                    LoteAccess.Instancia.ActualizarStockLote(StockActualizar, ref result, ref mensaje);
                                    break;
                                }
                                else
                                {
                                    CantidadReal = CantidadReal - itemLote.StockDisponible;
                                    VentaDetalle.cantidad = itemLote.StockDisponible;
                                    StockActualizar.StockDisponible = itemLote.StockDisponible;
                                    VentaAccess.Instancia.RegistrarVentaDetalle(VentaDetalle, ref result, ref mensaje);
                                    LoteAccess.Instancia.ActualizarStockLote(StockActualizar, ref result, ref mensaje);
                                    VentaDetalle.importe = 0;
                                }
                            }
                            else
                            {
                                scope.Dispose();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    scope.Dispose();
                }

                if (result)
                {
                    scope.Complete();
                }
                else
                {
                    scope.Dispose();
                }
            }
        }

    }
}
