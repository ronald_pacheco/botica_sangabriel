﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSG.DATAACCESS;
using BSG.DATACONTRACT;

namespace BSG.BUSSINESS
{
    public class ProductoBL
    {
        #region Singleton

        static ProductoBL() { }
        private ProductoBL() { }
        private static ProductoBL _instancia = new ProductoBL();
        public static ProductoBL Instancia { get { return _instancia; } }

        #endregion

        public List<UnidadMedidaBE> ObtenerUnidadMedida()
        {
            return ProductoAccess.Instancia.ObtenerUnidadMedida();
        }

        public ProductoBE RegistrarProducto(ProductoBE NuevoProducto, ref bool result, ref string mensaje)
        {
            return ProductoAccess.Instancia.RegistrarProducto(NuevoProducto, ref result, ref mensaje);
        }

        public List<ProductoBE> ObtenerProductos(ProductoBE Producto, ref bool result, ref string mensaje)
        {
            return ProductoAccess.Instancia.ObtenerProductos(Producto,ref result, ref mensaje);
        }

        public void EditarUsuario(ProductoBE EditProducto, ref bool result, ref string mensaje)
        {
            ProductoAccess.Instancia.EditarUsuario(EditProducto, ref result, ref mensaje);
        }

        public void EliminarProducto(ProductoBE EliminarProducto, ref bool result, ref string mensaje)
        {
            ProductoAccess.Instancia.EliminarProducto(EliminarProducto, ref result, ref mensaje);
        }
    }
}
