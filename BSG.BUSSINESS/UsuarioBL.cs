﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSG.DATACONTRACT;
using BSG.DATAACCESS;
using BSG.UTIL;

namespace BSG.BUSSINESS
{
    public class UsuarioBL
    {
        #region Singleton

        static UsuarioBL() { }
        private UsuarioBL() { }
        private static UsuarioBL _instancia = new UsuarioBL();
        public static UsuarioBL Instancia { get { return _instancia; } }

        #endregion

        public bool LoginUsuario(UsuarioBE LoginUser)
        {
            return UsuarioAccess.Instancia.LoginUsuario(LoginUser);
        }

        public UsuarioBE ObtenerUsuarioLogeado(UsuarioBE LoginUser)
        {
            return UsuarioAccess.Instancia.ObtenerUsuarioLogeado(LoginUser);
        }

        public List<ModuloBE> ObtenerModulos(UsuarioBE TipoUsuario)
        {
            return UsuarioAccess.Instancia.ObtenerModulos(TipoUsuario);
        }

        public UsuarioBE RegistrarUsuario(UsuarioBE NuevoUsuario, ref bool result, ref string mensaje)
        {
            UsuarioBE Usuario = null;
            try
            {
                Usuario = ObtenerUsuarios(NuevoUsuario).FirstOrDefault();
                if (Usuario == null)
                {
                    NuevoUsuario.password = SeguridadUtil.MD5Hash(NuevoUsuario.password);
                    Usuario = UsuarioAccess.Instancia.RegistrarUsuario(NuevoUsuario, ref result, ref mensaje);
                    Usuario = ObtenerUsuarios(Usuario).FirstOrDefault();
                }
                else
                {
                    throw new System.ArgumentException("el usuario ya Existe!","Error!");
                }
            }
            catch(Exception ex)
            {
                result = false;
                mensaje = "Ocurrio un error. por favor intentelo más tarde";
                mensaje = ex.Message;
            }
            return Usuario;
        }

        public void EditarUsuario(UsuarioBE EditarUsuario, ref bool result, ref string mensaje)
        {
            try
            {
                UsuarioAccess.Instancia.EditarUsuario(EditarUsuario, ref result, ref mensaje);
            }
            catch (Exception ex)
            {
                result = false;
                mensaje = "Ocurrio un error. por favor intentelo más tarde";
                mensaje = ex.Message;
            }
        }

        public void EliminarUsuario(UsuarioBE EliminarUsuario, ref bool result, ref string mensaje)
        {
            try
            {
                UsuarioAccess.Instancia.EliminarUsuario(EliminarUsuario, ref result, ref mensaje);
            }
            catch (Exception ex)
            {
                result = false;
                mensaje = "Ocurrio un error. por favor intentelo más tarde";
                mensaje = ex.Message;
            }
        }

        public List<UsuarioBE> ObtenerUsuarios(UsuarioBE Usuario)
        {
            return UsuarioAccess.Instancia.ObtenerUsuarios(Usuario);
        }

        public List<TipoUsuarioBE> ObtenerListaTipoUsuario()
        {
            List<TipoUsuarioBE> ListaTipoUsuario = new List<TipoUsuarioBE>();
            TipoUsuarioBE Admin = new TipoUsuarioBE();
            Admin.idTipoUsuario = 1;
            Admin.Descripcion = "Administrador";
            TipoUsuarioBE Tecnico = new TipoUsuarioBE();
            Tecnico.idTipoUsuario = 2;
            Tecnico.Descripcion = "Tecnico";

            ListaTipoUsuario.Add(Admin);
            ListaTipoUsuario.Add(Tecnico);

            return ListaTipoUsuario;
        }
    }
}
