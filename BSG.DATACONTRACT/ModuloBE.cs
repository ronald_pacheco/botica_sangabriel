﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class ModuloBE
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string URL { get; set; }
        public string icono { get; set; }
    }
}
