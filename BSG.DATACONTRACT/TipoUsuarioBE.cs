﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class TipoUsuarioBE
    {
        public int idTipoUsuario { get; set; }
        public string Descripcion { get; set; }
    }
}
