﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class ProductoBE
    {
        public int idProducto { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string laboratorio { get; set; }
        public string generico { get; set; }
        public int idUnidadMedida { get; set; }
        public string unidadMedida { get; set; }
        public decimal precioVenta { get; set; }
        public decimal precioCompra { get; set; }
        public int StockMinimo { get; set; }
        public int StockMostrador { get; set; }
        public int StockAlmacen { get; set; }
        public string descripcion { get; set; }
        public string ubicacion { get; set; }
        public string UsuarioMaster { get; set; }
        public int StockTotal { get; set; }
        public int StockDisponible { get; set; }
        public List<LoteBE> ListaLotes { get; set; }
    }
}
