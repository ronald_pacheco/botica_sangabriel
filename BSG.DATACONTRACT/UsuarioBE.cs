﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class UsuarioBE
    {
        public int idUsuario { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string usuario { get; set; }
        public string password { get; set; }
        public string DNI { get; set; }
        public string celular { get; set; }
        public string correo { get; set; }
        public int idTipoUsuario { get; set; }
        public string tipoUsuario { get; set; }
        public string estado { get; set; }
        public string MasterUsuario { get; set; }
    }
}
