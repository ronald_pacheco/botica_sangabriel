﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class LoteBE
    {
        public int idLote { get; set; }
        public string codigoLote { get; set; }
        public int idProducto { get; set; }
        public string codigoProducto { get; set; }
        public int StockAlmacen { get; set; }
        public int StockMostrador { get; set; }
        public int StockDisponible { get; set; }
        public int StockTotal { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public int cantidad { get; set; }
        public string nombre { get; set; }
        public string UsuarioMaster { get; set; }
        public string Buscar { get; set; }
        public int DiferenciaDias { get; set; }
        public string EstadoLote { get; set; }
    }
}
