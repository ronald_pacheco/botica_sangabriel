﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class VentaDetalleBE
    {
        public int idVentaDetalle { get; set; }
        public int idVenta { get; set; }
        public int idProducto { get; set; }
        public int idLote { get; set; }
        public int cantidad { get; set; }
        public decimal importe { get; set; }
    }
}
