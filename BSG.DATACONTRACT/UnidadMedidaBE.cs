﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class UnidadMedidaBE
    {
        public int idUnidadMedida { get; set; }
        public string UnidadMedida { get; set; }
    }
}
