﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSG.DATACONTRACT
{
    public class VentaBE
    {
        public int idVenta { get; set; }
        public int idProducto { get; set; }
        public int idLote { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public int Cantidadllevar { get; set; }
        public decimal precioVenta { get; set; }
        public int cantidad { get; set; }
        public string cliente { get; set; }
        public string estadoVenta { get; set; }
        public decimal subTotal { get; set; }
        public decimal importe { get; set; }
        public string UsuarioMaster { get; set; }
        public DateTime fechaVenta { get; set; }
        public ProductoBE Producto { get; set; }
        public List<ProductoBE> ListaProductos { get; set; }
        public List<UnidadMedidaBE> ListaUnidadMedida { get; set; }
        public List<PreVentaBE> ListaPreVenta { get; set; }
    }

    public class PreVentaBE
    {
        public int idProducto { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public int Cantidadllevar { get; set; }
        public decimal precioVenta { get; set; }
        public decimal SubTotal { get; set; }
    }
}
