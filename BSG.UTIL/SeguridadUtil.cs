﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace BSG.UTIL
{
    public class SeguridadUtil
    {
        public static string MD5Hash(string value)
        {
            if (value == null)
            {
                return value;
            }
            else
            {
                byte[] data = new MD5CryptoServiceProvider().ComputeHash(new UTF8Encoding().GetBytes(value));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                return sBuilder.ToString();
            }
        }
    }
}
