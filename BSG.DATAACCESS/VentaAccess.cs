﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using BSG.DATACONTRACT;

namespace BSG.DATAACCESS
{
    public class VentaAccess
    {
        #region Singleton

        static VentaAccess() { }
        private VentaAccess() { }
        string strConnString = ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString;
        private static VentaAccess _instancia = new VentaAccess();
        public static VentaAccess Instancia { get { return _instancia; } }

        #endregion

        public int RegistrarVenta(VentaBE NuevaVenta, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    int idVenta = 0;
                    SqlCommand cmd = new SqlCommand("SP_BSG_REGISTRAR_VENTA", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOCREADOR", NuevaVenta.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@IMPORTE", NuevaVenta.importe);
                    cmd.Parameters.AddWithValue("@CLIENTE", NuevaVenta.cliente.ToUpper());
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        if (!DBNull.Value.Equals(rdr["IDVENTA"])) idVenta = (int)rdr["IDVENTA"];
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Venta generada correctamente!";
                    return idVenta;
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                    return 0;
                }
            }
        }

        public void RegistrarVentaDetalle(VentaDetalleBE NuevaVentaDetalle, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    int idVenta = 0;
                    SqlCommand cmd = new SqlCommand("SP_BSG_REGISTRAR_VENTA_DETALLE", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDVENTA", NuevaVentaDetalle.idVenta);
                    cmd.Parameters.AddWithValue("@IDPRODUCTO", NuevaVentaDetalle.idProducto);
                    cmd.Parameters.AddWithValue("@IDLOTE", NuevaVentaDetalle.idLote);
                    cmd.Parameters.AddWithValue("@CANTIDAD", NuevaVentaDetalle.cantidad);
                    cmd.Parameters.AddWithValue("@IMPORTE", NuevaVentaDetalle.importe);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        if (!DBNull.Value.Equals(rdr["IDVENTADETALLE"])) idVenta = (int)rdr["IDVENTADETALLE"];
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Venta generada correctamente!";
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                }
            }
        }

    }
}
