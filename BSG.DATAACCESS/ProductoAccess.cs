﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using BSG.DATACONTRACT;

namespace BSG.DATAACCESS
{
    public class ProductoAccess
    {
        #region Singleton

        static ProductoAccess() { }
        private ProductoAccess() { }
        string strConnString = ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString;
        private static ProductoAccess _instancia = new ProductoAccess();
        public static ProductoAccess Instancia { get { return _instancia; } }

        #endregion

        public List<UnidadMedidaBE> ObtenerUnidadMedida()
        {
            List<UnidadMedidaBE> ListUnidadMedida = new List<UnidadMedidaBE>();

            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_OBTENER_UNIDAD_MEDIDA", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        UnidadMedidaBE UnidadMedida = new UnidadMedidaBE();
                        if (!DBNull.Value.Equals(rdr["IDUNIDADMEDIDA"])) UnidadMedida.idUnidadMedida = (int)rdr["IDUNIDADMEDIDA"];
                        if (!DBNull.Value.Equals(rdr["UNIDADMEDIDA"])) UnidadMedida.UnidadMedida = (string)rdr["UNIDADMEDIDA"];
                        ListUnidadMedida.Add(UnidadMedida);
                    }
                    conexion.Close();
                }
                catch (Exception ex)
                {
                    ListUnidadMedida = null;
                }
            }

            return ListUnidadMedida;
        }

        public ProductoBE RegistrarProducto(ProductoBE NuevoProducto, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    ProductoBE ProductoCreado = new ProductoBE();
                    SqlCommand cmd = new SqlCommand("SP_BSG_REGISTRAR_PRODUCTO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOCREADOR", NuevoProducto.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@NOMBRE", NuevoProducto.nombre.ToUpper());
                    cmd.Parameters.AddWithValue("@LABORATORIO", NuevoProducto.laboratorio.ToUpper());
                    cmd.Parameters.AddWithValue("@GENERICO", NuevoProducto.generico.ToUpper());
                    cmd.Parameters.AddWithValue("@ID_UNIDADMEDIDA", NuevoProducto.idUnidadMedida);
                    cmd.Parameters.AddWithValue("@PRECIOVENTA", NuevoProducto.precioVenta);
                    cmd.Parameters.AddWithValue("@PRECIOCOMPRA", NuevoProducto.precioCompra);
                    cmd.Parameters.AddWithValue("@STOCKMINIMO", NuevoProducto.StockMinimo);
                    cmd.Parameters.AddWithValue("@UBICACION", NuevoProducto.ubicacion);
                    cmd.Parameters.AddWithValue("@DESCRIPCION", NuevoProducto.descripcion);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        if (!DBNull.Value.Equals(rdr["IDPRODUCTO"])) ProductoCreado.idProducto = (int)rdr["IDPRODUCTO"];
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Producto creado correctamente!";
                    return ProductoCreado;
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                    return null;
                }
            }
        }

        public List<ProductoBE> ObtenerProductos(ProductoBE Producto, ref bool result, ref string mensaje)
        {
            List<ProductoBE> ListProducto = new List<ProductoBE>();

            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_OBTENER_PRODUCTO", conexion);
                    cmd.Parameters.AddWithValue("@IDPRODUCTO", Producto.idProducto);
                    cmd.Parameters.AddWithValue("@CODIGO", Producto.codigo);
                    cmd.CommandType = CommandType.StoredProcedure;
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        ProductoBE Prd = new ProductoBE();
                        if (!DBNull.Value.Equals(rdr["IDPRODUCTO"])) Prd.idProducto = (int)rdr["IDPRODUCTO"];
                        if (!DBNull.Value.Equals(rdr["CODIGO"])) Prd.codigo = (string)rdr["CODIGO"];
                        if (!DBNull.Value.Equals(rdr["NOMBRE"])) Prd.nombre = (string)rdr["NOMBRE"];
                        if (!DBNull.Value.Equals(rdr["LABORATORIO"])) Prd.laboratorio = (string)rdr["LABORATORIO"];
                        if (!DBNull.Value.Equals(rdr["GENERICO"])) Prd.generico = (string)rdr["GENERICO"];
                        if (!DBNull.Value.Equals(rdr["IDUNIDADMEDIDA"])) Prd.idUnidadMedida = (int)rdr["IDUNIDADMEDIDA"];
                        if (!DBNull.Value.Equals(rdr["UNIDADMEDIDA"])) Prd.unidadMedida = (string)rdr["UNIDADMEDIDA"];
                        if (!DBNull.Value.Equals(rdr["PRECIOVENTA"])) Prd.precioVenta = (decimal)rdr["PRECIOVENTA"];
                        if (!DBNull.Value.Equals(rdr["PRECIOCOMPRA"])) Prd.precioCompra = (decimal)rdr["PRECIOCOMPRA"];
                        if (!DBNull.Value.Equals(rdr["STOCKMINIMO"])) Prd.StockMinimo = (int)rdr["STOCKMINIMO"];
                        if (!DBNull.Value.Equals(rdr["DESCRIPCION"])) Prd.descripcion = (string)rdr["DESCRIPCION"];
                        if (!DBNull.Value.Equals(rdr["UBICACION"])) Prd.ubicacion = (string)rdr["UBICACION"];
                        //if (!DBNull.Value.Equals(rdr["STOCKMOSTRADOR"])) Prd.StockMostrador = (int)rdr["STOCKMOSTRADOR"];
                        //if (!DBNull.Value.Equals(rdr["STOCKALMACEN"])) Prd.StockAlmacen = (int)rdr["STOCKALMACEN"];
                        //if (!DBNull.Value.Equals(rdr["STOCKTOTAL"])) Prd.StockTotal = (int)rdr["STOCKTOTAL"];
                        if (!DBNull.Value.Equals(rdr["STOCKDISPONIBLE"])) Prd.StockDisponible = (int)rdr["STOCKDISPONIBLE"];
                        ListProducto.Add(Prd);
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Se obtuvo la lista de Productos correctamente.";
                }
                catch (Exception ex)
                {
                    ListProducto = null;
                    result = false;
                    mensaje = ex.Message;
                }
            }

            return ListProducto;
        }

        public void EditarUsuario(ProductoBE EditProducto, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_EDITAR_PRODUCTO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOMODIFICADOR", EditProducto.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@IDPRODUCTO", EditProducto.idProducto);
                    cmd.Parameters.AddWithValue("@NOMBRE", EditProducto.nombre.ToUpper());
                    cmd.Parameters.AddWithValue("@LABORATORIO", EditProducto.laboratorio.ToUpper());
                    cmd.Parameters.AddWithValue("@GENERICO", EditProducto.generico.ToUpper());
                    cmd.Parameters.AddWithValue("@ID_UNIDADMEDIDA", EditProducto.idUnidadMedida);
                    cmd.Parameters.AddWithValue("@PRECIOVENTA", EditProducto.precioVenta);
                    cmd.Parameters.AddWithValue("@PRECIOCOMPRA", EditProducto.precioCompra);
                    cmd.Parameters.AddWithValue("@STOCKMINIMO", EditProducto.StockMinimo);
                    cmd.Parameters.AddWithValue("@UBICACION", EditProducto.ubicacion);
                    cmd.Parameters.AddWithValue("@DESCRIPCION", EditProducto.descripcion);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Producto editado correctamente!";
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                }
            }
        }

        public void EliminarProducto(ProductoBE EliminarProducto, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_ELIMINAR_PRODUCTO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOMODIFICADOR", EliminarProducto.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@ID_PRODUCTO", EliminarProducto.idProducto);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Producto Eliminado correctamente!";
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                }
            }
        }
    }
}
