﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using BSG.DATACONTRACT;

namespace BSG.DATAACCESS
{
    public class LoteAccess
    {
        #region Singleton

        static LoteAccess() { }
        private LoteAccess() { }
        string strConnString = ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString;
        private static LoteAccess _instancia = new LoteAccess();
        public static LoteAccess Instancia { get { return _instancia; } }

        #endregion

        public LoteBE RegistrarLote(LoteBE NuevoLote, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    LoteBE LoteCreado = new LoteBE();
                    SqlCommand cmd = new SqlCommand("SP_BSG_REGISTRAR_LOTE", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOCREADOR", NuevoLote.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@CODIGOLOTE", NuevoLote.codigoLote);
                    cmd.Parameters.AddWithValue("@IDPRODUCTO", NuevoLote.idProducto);
                    cmd.Parameters.AddWithValue("@STOCKALMACEN", NuevoLote.StockAlmacen);
                    cmd.Parameters.AddWithValue("@STOCKMOSTRADOR", NuevoLote.StockMostrador);
                    cmd.Parameters.AddWithValue("@FECHAVENCIMIENTO", NuevoLote.fechaVencimiento);
                    cmd.Parameters.AddWithValue("@STOCKTOTAL", NuevoLote.StockAlmacen + NuevoLote.StockMostrador);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        if (!DBNull.Value.Equals(rdr["IDLOTE"])) LoteCreado.idLote = (int)rdr["IDLOTE"];
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Lote creado correctamente!";
                    return LoteCreado;
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                    return null;
                }
            }
        }

        public List<LoteBE> ObtenerLotes(LoteBE Lote, ref bool result, ref string mensaje)
        {
            List<LoteBE> ListLote = new List<LoteBE>();

            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_OBTENER_LOTE", conexion);
                    cmd.Parameters.AddWithValue("@IDLOTE", Lote.idLote);
                    cmd.Parameters.AddWithValue("@CODIGOLOTE", Lote.codigoLote);
                    cmd.Parameters.AddWithValue("@IDPRODUCTO", Lote.idProducto);
                    cmd.Parameters.AddWithValue("@ESTADO", Lote.EstadoLote);
                    cmd.CommandType = CommandType.StoredProcedure;
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        LoteBE Lot = new LoteBE();
                        if (!DBNull.Value.Equals(rdr["IDLOTE"])) Lot.idLote = (int)rdr["IDLOTE"];
                        if (!DBNull.Value.Equals(rdr["CODIGOLOTE"])) Lot.codigoLote = (string)rdr["CODIGOLOTE"];
                        if (!DBNull.Value.Equals(rdr["CODIGOPRODUCTO"])) Lot.codigoProducto = (string)rdr["CODIGOPRODUCTO"];
                        if (!DBNull.Value.Equals(rdr["NOMBRE"])) Lot.nombre = (string)rdr["NOMBRE"];
                        if (!DBNull.Value.Equals(rdr["IDPRODUCTO"])) Lot.idProducto = (int)rdr["IDPRODUCTO"];
                        if (!DBNull.Value.Equals(rdr["FECHAVENCIMIENTO"])) Lot.fechaVencimiento = Convert.ToDateTime(((DateTime)rdr["FECHAVENCIMIENTO"]).ToShortDateString());
                        if (!DBNull.Value.Equals(rdr["STOCKMOSTRADOR"])) Lot.StockMostrador = (int)rdr["STOCKMOSTRADOR"];
                        if (!DBNull.Value.Equals(rdr["STOCKALMACEN"])) Lot.StockAlmacen = (int)rdr["STOCKALMACEN"];
                        if (!DBNull.Value.Equals(rdr["DIFERENCIADIAS"])) Lot.DiferenciaDias = (int)rdr["DIFERENCIADIAS"];
                        if (!DBNull.Value.Equals(rdr["STOCKDISPONIBLE"])) Lot.StockDisponible = (int)rdr["STOCKDISPONIBLE"];
                        if (!DBNull.Value.Equals(rdr["STOCKTOTAL"])) Lot.StockTotal = (int)rdr["STOCKTOTAL"];
                        if (!DBNull.Value.Equals(rdr["ESTADO_LOTE"])) Lot.EstadoLote = (string)rdr["ESTADO_LOTE"];
                        ListLote.Add(Lot);
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Se obtuvo la lista de Lotes correctamente.";
                }
                catch (Exception ex)
                {
                    ListLote = null;
                    result = false;
                    mensaje = ex.Message;
                }
            }
            return ListLote;
        }

        public LoteBE EditarLote(LoteBE EditarLote, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    LoteBE LoteCreado = new LoteBE();
                    SqlCommand cmd = new SqlCommand("SP_BSG_EDITAR_LOTE", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOMODIFICADOR", EditarLote.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@IDLOTE", EditarLote.idLote);
                    cmd.Parameters.AddWithValue("@CODIGOLOTE", EditarLote.codigoLote);
                    cmd.Parameters.AddWithValue("@STOCKMOSTRADOR", EditarLote.StockMostrador);
                    cmd.Parameters.AddWithValue("@STOCKALMACEN", EditarLote.StockAlmacen);
                    cmd.Parameters.AddWithValue("@FECHAVENCIMIENTO", EditarLote.fechaVencimiento);
                    cmd.Parameters.AddWithValue("@STOCKTOTAL", EditarLote.StockAlmacen + EditarLote.StockMostrador);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Lote Editado correctamente!";
                    return LoteCreado;
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                    return null;
                }
            }
        }

        public LoteBE EliminarLote(LoteBE EliminarLote, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    LoteBE LoteCreado = new LoteBE();
                    SqlCommand cmd = new SqlCommand("SP_BSG_ELIMINAR_LOTE", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOMODIFICADOR", EliminarLote.UsuarioMaster.ToUpper());
                    cmd.Parameters.AddWithValue("@IDLOTE", EliminarLote.idLote);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Lote Eliminado correctamente!";
                    return LoteCreado;
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                    return null;
                }
            }
        }

        public void ActualizarStockLote(LoteBE EditarLote, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    LoteBE LoteCreado = new LoteBE();
                    SqlCommand cmd = new SqlCommand("SP_BSG_ACTUALIZAR_LOTE", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDLOTE", EditarLote.idLote);
                    cmd.Parameters.AddWithValue("@STOCKDISPONIBLE", EditarLote.StockDisponible);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Stock Editado correctamente!";
                    //return t;
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                    //return null;
                }
            }
        }

    }
}
