﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
//using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Configuration;
using BSG.DATACONTRACT;

namespace BSG.DATAACCESS
{
    public class UsuarioAccess
    {
        #region Singleton

        static UsuarioAccess() { }
        private UsuarioAccess() { }
        string strConnString = ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString;
        private static UsuarioAccess _instancia = new UsuarioAccess();
        public static UsuarioAccess Instancia { get { return _instancia; } }

        #endregion

        public bool LoginUsuario(UsuarioBE LoginUser)
        {
            bool UserLogeado = false;
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_LOGIN_USUARIO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIO", LoginUser.usuario);
                    cmd.Parameters.AddWithValue("@PASSWORD", LoginUser.password);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        UserLogeado = true;
                    }
                    conexion.Close();
                }
                catch(Exception ex)
                {
                    UserLogeado = false;
                }
            }
            return UserLogeado;
        }

        public UsuarioBE ObtenerUsuarioLogeado(UsuarioBE LoginUser)
        {
            UsuarioBE UserLogeado = null;
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("[SP_BSG_OBTENER_USUARIO_LOGEADO]", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIO", LoginUser.usuario);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        UserLogeado = new UsuarioBE();
                        if (!DBNull.Value.Equals(rdr["IDUSUARIO"])) UserLogeado.idUsuario = (int)rdr["IDUSUARIO"];
                        if (!DBNull.Value.Equals(rdr["NOMBRE"])) UserLogeado.nombre = (string)rdr["NOMBRE"];
                        if (!DBNull.Value.Equals(rdr["APELLIDO"])) UserLogeado.apellido = (string)rdr["APELLIDO"];
                        if (!DBNull.Value.Equals(rdr["USUARIO"])) UserLogeado.usuario = (string)rdr["USUARIO"];
                        if (!DBNull.Value.Equals(rdr["DNI"])) UserLogeado.DNI = (string)rdr["DNI"];
                        if (!DBNull.Value.Equals(rdr["CELULAR"])) UserLogeado.celular = (string)rdr["CELULAR"];
                        if (!DBNull.Value.Equals(rdr["CORREO"])) UserLogeado.correo = (string)rdr["CORREO"];
                        if (!DBNull.Value.Equals(rdr["ID_01_TIPOUSUARIO"])) UserLogeado.idTipoUsuario = (int)rdr["ID_01_TIPOUSUARIO"];
                        if (!DBNull.Value.Equals(rdr["DESCRIPCION"])) UserLogeado.tipoUsuario = (string)rdr["DESCRIPCION"];
                    }
                    conexion.Close();
                }
                catch (Exception ex)
                {
                    UserLogeado = null;
                }
            }

            return UserLogeado;
        }

        public List<ModuloBE> ObtenerModulos(UsuarioBE TipoUsuario)
        {
            List<ModuloBE> ListaModulos = new List<ModuloBE>();
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_OBTENER_MODULOS", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TIPOUSUARIO", TipoUsuario.idTipoUsuario);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        ModuloBE Modulo = new ModuloBE();
                        if (!DBNull.Value.Equals(rdr["NOMBRE"])) Modulo.nombre = (string)rdr["NOMBRE"];
                        if (!DBNull.Value.Equals(rdr["DESCRIPCION"])) Modulo.descripcion = (string)rdr["DESCRIPCION"];
                        if (!DBNull.Value.Equals(rdr["URL"])) Modulo.URL = (string)rdr["URL"];
                        if (!DBNull.Value.Equals(rdr["ICONO"])) Modulo.icono = (string)rdr["ICONO"];
                        ListaModulos.Add(Modulo);
                    }
                    conexion.Close();
                }
                catch (Exception ex)
                {
                    ListaModulos = null;
                }
            }
            return ListaModulos;
        }

        public UsuarioBE RegistrarUsuario(UsuarioBE NuevoUsuario, ref bool result, ref string mensaje)
        {
            UsuarioBE UsuarioRegistrado = NuevoUsuario;
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_REGISTRAR_USUARIO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOCREADOR", NuevoUsuario.MasterUsuario.ToUpper());
                    cmd.Parameters.AddWithValue("@USUARIO", NuevoUsuario.usuario.ToUpper());
                    cmd.Parameters.AddWithValue("@PASSWORD", NuevoUsuario.password);
                    cmd.Parameters.AddWithValue("@NOMBRE", NuevoUsuario.nombre.ToUpper());
                    cmd.Parameters.AddWithValue("@APELLIDO", NuevoUsuario.apellido.ToUpper());
                    cmd.Parameters.AddWithValue("@DNI", NuevoUsuario.DNI);
                    cmd.Parameters.AddWithValue("@CELULAR", NuevoUsuario.celular);
                    cmd.Parameters.AddWithValue("@CORREO", NuevoUsuario.correo);
                    cmd.Parameters.AddWithValue("@ID_TIPOUSUARIO", NuevoUsuario.idTipoUsuario);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        if (!DBNull.Value.Equals(rdr["IDUSUARIO"])) UsuarioRegistrado.idUsuario = (int)rdr["IDUSUARIO"];
                    }
                    conexion.Close();
                    result = true;
                    mensaje = "Usuario creado correctamente!";
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                }
            }
            return UsuarioRegistrado;
        }

        public void EditarUsuario(UsuarioBE EditarUsuario, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_EDITAR_USUARIO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOMODIFICADOR", EditarUsuario.MasterUsuario.ToUpper());
                    cmd.Parameters.AddWithValue("@ID_USUARIO", EditarUsuario.idUsuario);
                    cmd.Parameters.AddWithValue("@USUARIO", EditarUsuario.usuario.ToUpper());
                    cmd.Parameters.AddWithValue("@NOMBRE", EditarUsuario.nombre.ToUpper());
                    cmd.Parameters.AddWithValue("@APELLIDO", EditarUsuario.apellido.ToUpper());
                    cmd.Parameters.AddWithValue("@DNI", EditarUsuario.DNI);
                    cmd.Parameters.AddWithValue("@CELULAR", EditarUsuario.celular);
                    cmd.Parameters.AddWithValue("@CORREO", EditarUsuario.correo);
                    cmd.Parameters.AddWithValue("@ID_TIPOUSUARIO", EditarUsuario.idTipoUsuario);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Usuario Editado correctamente!";
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                }
            }
        }

        public void EliminarUsuario(UsuarioBE EliminarUsuario, ref bool result, ref string mensaje)
        {
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_ELIMINAR_USUARIO", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIOMODIFICADOR", EliminarUsuario.MasterUsuario.ToUpper());
                    cmd.Parameters.AddWithValue("@ID_USUARIO", EliminarUsuario.idUsuario);
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    conexion.Close();
                    result = true;
                    mensaje = "Usuario Eliminado correctamente!";
                }
                catch (Exception ex)
                {
                    result = false;
                    mensaje = "Ocurrio un error. por favor intentelo más tarde";
                    mensaje = ex.Message;
                }
            }
        }

        public List<UsuarioBE> ObtenerUsuarios(UsuarioBE Usuario)
        {
            List<UsuarioBE> ListUsuarios = new List<UsuarioBE>();
            using (SqlConnection conexion = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BSG_OBTENER_USUARIO", conexion);
                    cmd.Parameters.AddWithValue("@USUARIO", Usuario.usuario);
                    cmd.CommandType = CommandType.StoredProcedure;
                    conexion.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        UsuarioBE User = new UsuarioBE();
                        if (!DBNull.Value.Equals(rdr["IDUSUARIO"])) User.idUsuario = (int)rdr["IDUSUARIO"];
                        if (!DBNull.Value.Equals(rdr["NOMBRE"])) User.nombre = (string)rdr["NOMBRE"];
                        if (!DBNull.Value.Equals(rdr["APELLIDO"])) User.apellido = (string)rdr["APELLIDO"];
                        if (!DBNull.Value.Equals(rdr["USUARIO"])) User.usuario = (string)rdr["USUARIO"];
                        if (!DBNull.Value.Equals(rdr["DNI"])) User.DNI = (string)rdr["DNI"];
                        if (!DBNull.Value.Equals(rdr["CELULAR"])) User.celular = (string)rdr["CELULAR"];
                        if (!DBNull.Value.Equals(rdr["CORREO"])) User.correo = (string)rdr["CORREO"];
                        if (!DBNull.Value.Equals(rdr["ID_01_TIPOUSUARIO"])) User.idTipoUsuario = (int)rdr["ID_01_TIPOUSUARIO"];
                        if (!DBNull.Value.Equals(rdr["DESCRIPCION"])) User.tipoUsuario = (string)rdr["DESCRIPCION"];
                        ListUsuarios.Add(User);
                    }
                    conexion.Close();
                }
                catch (Exception ex)
                {
                    ListUsuarios = null;
                }
            }
            return ListUsuarios;
        }
    }
}
