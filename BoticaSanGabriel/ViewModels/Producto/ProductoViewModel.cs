﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.ViewModels.Producto
{
    public class ProductoViewModel
    {
        public ProductoBE Producto { get; set; }
        public List<ProductoBE> ListaProductos { get; set; }
        public List<UnidadMedidaBE> ListUnidadMedida { get; set; }
    }
}