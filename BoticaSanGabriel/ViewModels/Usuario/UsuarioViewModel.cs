﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.ViewModels.Usuario
{
    public class UsuarioViewModel
    {
        public int idUsuario { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string usuario { get; set; }
        public string password { get; set; }
        public string DNI { get; set; }
        public string celular { get; set; }
        public string correo { get; set; }
        public int idTipoUsuario { get; set; }
        public string tipoUsuario { get; set; }
        public string estado { get; set; }
        public UsuarioBE ObjUsuario { get; set; }
        public List<UsuarioBE> ListaUsuarios { get; set; }
        public List<TipoUsuarioBE> ListaTipoUsuario { get; set; }
    }
}