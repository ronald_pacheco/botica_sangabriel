﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.ViewModels.Util
{
    public class InicioViewModel
    {
        public List<ModuloBE> ListaModulos { get; set; }
        public ModuloBE ModuloActual { get; set; }

    }
}