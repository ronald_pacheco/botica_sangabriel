﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace BoticaSanGabriel.ViewModels.Seguridad
{
    [DataContract]
    [DebuggerDisplay("Usuario = {Usuario}, Codigo = {Codigo}")]
    public class LoginViewModel
    {
        [DataMember]
        [DataType(DataType.Text)]
        [DisplayName("Usuario")]
        [Required(ErrorMessage = "*")]
        public string Usuario { get; set; }

        [DataMember]
        [DataType(DataType.Text)]
        [DisplayName("Password")]
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }

        [DataMember]
        [DisplayName("Fecha de Sistema")]
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }

        public int IdentEmpresa { get; set; }

        public LoginViewModel()
        {
            Fecha = DateTime.Now;
        }
    }
}