﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.ViewModels.Venta
{
    public class VentaViewModel
    {
        public VentaBE Venta { get; set; }
        public ProductoBE Producto { get; set; }
        public List<PreVentaBE> ListaPreVenta { get; set; }
        public string Cliente { get; set; }
    }
}