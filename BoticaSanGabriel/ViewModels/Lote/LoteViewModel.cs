﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.ViewModels.Lote
{
    public class LoteViewModel
    {
        public ProductoBE Producto { get; set; }
        public List<ProductoBE> ListaProductos { get; set; }
        public LoteBE Lote { get; set; }
        public List<LoteBE> ListaLotes { get; set; }
    }
}