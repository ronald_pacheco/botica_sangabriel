﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoticaSanGabriel.ViewModels.Prueba
{
    public class PruebaViewModel
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
        public List<Paises> ListaPaises { get; set; }
        public List<contacto> contacto { get; set; }
    }

    public class contacto
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public List<phone> phones { get; set; }
    }

    public class phone
    {
        public string type { get; set; }
        public string number { get; set; }
    }

    public class Paises
    {
        public int idPais { get; set; }
        public string Pais { get; set; }
        public string Continente { get; set; }
    }
}