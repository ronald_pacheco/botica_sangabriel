﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoticaSanGabriel.ViewModels.ViewEngine
{
    public class ViewEngine : RazorViewEngine
    {
        public ViewEngine()
        {
            string[] viewLocationFormatArr = new string[8];
            viewLocationFormatArr[0] = "~/Views/Seguridad/{1}/{0}.cshtml";
            viewLocationFormatArr[1] = "~/Views/Util/{1}/{0}.cshtml";
            viewLocationFormatArr[2] = "~/Views/Shared/{0}.cshtml";
            viewLocationFormatArr[3] = "~/Views/Usuario/{1}/{0}.cshtml";
            viewLocationFormatArr[4] = "~/Views/Prueba/{1}/{0}.cshtml";
            viewLocationFormatArr[5] = "~/Views/Producto/{1}/{0}.cshtml";
            viewLocationFormatArr[6] = "~/Views/Lote/{1}/{0}.cshtml";
            viewLocationFormatArr[7] = "~/Views/Venta/{1}/{0}.cshtml";
            this.ViewLocationFormats = viewLocationFormatArr;
            string[] partialViewLocationFormatArr = new string[8];
            partialViewLocationFormatArr[0] = "~/Views/Seguridad/{1}/{0}.cshtml";
            partialViewLocationFormatArr[1] = "~/Views/Util/{1}/{0}.cshtml";
            partialViewLocationFormatArr[2] = "~/Views/Shared/{0}.cshtml";
            partialViewLocationFormatArr[3] = "~/Views/Usuario/{1}/{0}.cshtml";
            partialViewLocationFormatArr[4] = "~/Views/Prueba/{1}/{0}.cshtml";
            partialViewLocationFormatArr[5] = "~/Views/Producto/{1}/{0}.cshtml";
            partialViewLocationFormatArr[6] = "~/Views/Lote/{1}/{0}.cshtml";
            partialViewLocationFormatArr[7] = "~/Views/Venta/{1}/{0}.cshtml";
            this.ViewLocationFormats = partialViewLocationFormatArr;
        }
    }
}