﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BoticaSanGabriel.ViewModels.ViewEngine;

namespace BoticaSanGabriel
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("Modulo", "{module}",
                new { controller = "Util", action = "Inicio" });
            routes.MapRoute("Default", "{module}/{controller}/{action}",
                new { module = "Seguridad", controller = "Login", action = "Login" });
            routes.MapRoute("SubModulo", "{module}/{submodule}/{controller}/{action}/{id}",
                new { module = "Seguridad", controller = "Login", action = "Login", id = UrlParameter.Optional });
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            var ViewEngineS = new ViewEngine();
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(ViewEngineS);
        }
    }
}