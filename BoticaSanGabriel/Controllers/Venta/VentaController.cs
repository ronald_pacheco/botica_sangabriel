﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoticaSanGabriel.ViewModels.Venta;
using BSG.BUSSINESS;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.Controllers.Venta
{
    public class VentaController : Controller
    {
        //
        // GET: /Venta/

        public ActionResult Index()
        {
            bool result = false;
            string mensaje = string.Empty;
            VentaViewModel model = new VentaViewModel();
            model.Venta = new VentaBE();
            model.Venta.ListaUnidadMedida = ProductoBL.Instancia.ObtenerUnidadMedida();
            model.Venta.ListaProductos = ProductoBL.Instancia.ObtenerProductos(new ProductoBE(), ref result, ref mensaje);
            model.Venta.ListaPreVenta = new List<PreVentaBE>();
            return View(model);
        }

        public JsonResult ObtenerLote(VentaViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            LoteBE LoteBuscar = new LoteBE();
            LoteBuscar.idProducto = model.Producto.idProducto;
            List<LoteBE> ListaLotes = LoteBL.Instancia.ObtenerLotes(LoteBuscar, ref result, ref mensaje);
            return Json(new { ok = result, data = ListaLotes, mensaje = mensaje });
        }

        public JsonResult ConvertirObjecto(PreVentaBE preventa)
        {
            return Json(new { data = preventa });
        }

        public JsonResult GenerarVenta(VentaViewModel Venta)
        {
            bool result = false;
            string mensaje = string.Empty;
            VentaBE VentaGen = new VentaBE();
            VentaGen.ListaPreVenta = Venta.ListaPreVenta;
            VentaGen.UsuarioMaster = Session["Usuario"].ToString();
            VentaGen.cliente = Venta.Cliente;
            VentaBL.Instancia.GenerarVenta(VentaGen, ref result, ref mensaje);

            return Json(new { ok = result });
        }

    }
}
