﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BSG.DATACONTRACT;
using BoticaSanGabriel.ViewModels.Usuario;
using BSG.BUSSINESS;

namespace BoticaSanGabriel.Controllers.Usuario
{
    public class UsuarioController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            UsuarioViewModel model = new UsuarioViewModel();
            model.ObjUsuario = new UsuarioBE();
            model.ListaTipoUsuario = UsuarioBL.Instancia.ObtenerListaTipoUsuario();
            model.ListaUsuarios = UsuarioBL.Instancia.ObtenerUsuarios(new UsuarioBE());
            return View(model);
        }

        [HttpPost]
        public JsonResult RegistrarUsuario(UsuarioViewModel NuevoUsuario)
        {
            bool result = false;
            string mensaje = string.Empty;
            UsuarioBE UsuarioNuevo = new UsuarioBE();
            NuevoUsuario.ObjUsuario.MasterUsuario = Session["Usuario"].ToString();
            UsuarioNuevo = UsuarioBL.Instancia.RegistrarUsuario(NuevoUsuario.ObjUsuario, ref result, ref mensaje);
            return Json(new { data = UsuarioNuevo, ok = result, mensaje = mensaje });
        }

        [HttpPost]
        public JsonResult EditarUsuario(UsuarioViewModel EditarUsuario)
        {
            bool result = false;
            string mensaje = string.Empty;
            UsuarioBE UsuarioNuevo = new UsuarioBE();
            EditarUsuario.ObjUsuario.MasterUsuario = Session["Usuario"].ToString();
            UsuarioBL.Instancia.EditarUsuario(EditarUsuario.ObjUsuario,ref result, ref mensaje);
            return Json(new { ok = result, mensaje = mensaje });
        }

        [HttpPost]
        public JsonResult EliminarUsuario(UsuarioViewModel EliminarUsuario)
        {
            bool result = false;
            string mensaje = string.Empty;
            UsuarioBE UsuarioNuevo = new UsuarioBE();
            EliminarUsuario.ObjUsuario.MasterUsuario = Session["Usuario"].ToString();
            UsuarioBL.Instancia.EliminarUsuario(EliminarUsuario.ObjUsuario, ref result, ref mensaje);
            return Json(new { ok = result, mensaje = mensaje });
        }
    }
}
