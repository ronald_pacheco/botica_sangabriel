﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoticaSanGabriel.ViewModels.Producto;
using BSG.BUSSINESS;
using BSG.DATACONTRACT;

namespace BoticaSanGabriel.Controllers.Producto
{
    public class ProductoController : Controller
    {
        //
        // GET: /Producto/
        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            bool result = false;
            string mensaje = string.Empty;
            ProductoViewModel model = new ProductoViewModel();
            model.ListUnidadMedida = ProductoBL.Instancia.ObtenerUnidadMedida();
            model.ListaProductos = ProductoBL.Instancia.ObtenerProductos(new ProductoBE(),ref result,ref mensaje);
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Registrar()
        {
            ProductoViewModel model = new ProductoViewModel();
            model.ListUnidadMedida = ProductoBL.Instancia.ObtenerUnidadMedida();
            return View(model);
        }

        [HttpPost]
        public JsonResult Registrar(ProductoViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            ProductoBE ProductoNuevo = new ProductoBE();
            model.Producto.UsuarioMaster = Session["Usuario"].ToString();
            ProductoNuevo = ProductoBL.Instancia.RegistrarProducto(model.Producto, ref result, ref mensaje);
            return Json(new { ok = result, data = ProductoNuevo, mensaje = mensaje });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Editar(int idProducto)
        {
            bool result = false;
            string mensaje = string.Empty;
            ProductoViewModel model = new ProductoViewModel();
            ProductoBE producto = new ProductoBE();
            producto.idProducto = idProducto;
            model.Producto = ProductoBL.Instancia.ObtenerProductos(producto, ref result, ref mensaje).FirstOrDefault();
            model.ListUnidadMedida = ProductoBL.Instancia.ObtenerUnidadMedida();
            return View(model);
        }

        [HttpPost]
        public JsonResult Editar(ProductoViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            model.Producto.UsuarioMaster = Session["Usuario"].ToString();
            ProductoBL.Instancia.EditarUsuario(model.Producto, ref result, ref mensaje);
            return Json(new { ok = result, mensaje = mensaje });
        }

        public JsonResult Eliminar(ProductoViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            model.Producto.UsuarioMaster = Session["Usuario"].ToString();
            ProductoBL.Instancia.EliminarProducto(model.Producto, ref result, ref mensaje);
            return Json(new { ok = result, mensaje = mensaje });
        }

    }
}
