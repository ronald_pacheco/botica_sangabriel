﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoticaSanGabriel.ViewModels.Prueba;

namespace BoticaSanGabriel.Controllers.Prueba
{
    public class PruebaController : Controller
    {
        //
        // GET: /Prueba/

        public ActionResult Index()
        {
            PruebaViewModel model = new PruebaViewModel();

            model.nombre = "Ronald";
            model.apellido = "Pacheco Alarcón";
            model.ListaPaises = new List<Paises>();
            Paises Pais1 = new Paises();
            Pais1.idPais = 1;
            Pais1.Pais = "Perú";
            Pais1.Continente = "America";
            model.ListaPaises.Add(Pais1);

            Paises Pais2 = new Paises();
            Pais2.idPais = 2;
            Pais2.Pais = "Francia";
            Pais2.Continente = "Europa";
            model.ListaPaises.Add(Pais2);

            //var initialData = [
            //                    {
            //                        firstName: "Danny", lastName: "LaRusso", phones: [
            //                          { type: "Mobile", number: "(555) 121-2121" },
            //                          { type: "Home", number: "(555) 123-4567" }]
            //                    },
            //                    {
            //                        firstName: "Sensei", lastName: "Miyagi", phones: [
            //                          { type: "Mobile", number: "(555) 444-2222" },
            //                          { type: "Home", number: "(555) 999-1212" }]
            //                    }
            //                    ];

            //model.contacto = new List<contacto>();

            //contacto contacto1 = new contacto();
            //contacto1.firstName = "Ronald";
            //contacto1.lastName = "Pacheco Alarcón";
            //contacto1.phones = new List<phone>();
            //phone phone11 = new phone();
            //phone11.type = "MOBIL";
            //phone11.number = "0939393933";
            //contacto1.phones.Add(phone11);
            //model.contacto.Add(contacto1);

            //contacto contacto12 = new contacto();
            //contacto12.firstName = "Soledad";
            //contacto12.lastName = "Nuñez de La Cruz";
            //contacto12.phones = new List<phone>();
            //phone phone12 = new phone();
            //phone12.type = "Casa";
            //phone12.number = "96323193892";
            //contacto12.phones.Add(phone12);
            //model.contacto.Add(contacto12);

            return View(model);
        }
    }
}
