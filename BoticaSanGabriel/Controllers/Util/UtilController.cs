﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BSG.BUSSINESS;
using BSG.DATACONTRACT;
using BoticaSanGabriel.ViewModels.Util;

namespace BoticaSanGabriel.Controllers.Util
{
    public class UtilController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Inicio()
        {
            UsuarioBE UsuarioLogeado = new UsuarioBE();
            InicioViewModel model = new InicioViewModel();
            try
            {
                UsuarioLogeado.usuario = Session["Usuario"].ToString();
                UsuarioLogeado = UsuarioBL.Instancia.ObtenerUsuarioLogeado(UsuarioLogeado);
                List<ModuloBE> ListaModulos = UsuarioBL.Instancia.ObtenerModulos(UsuarioLogeado);
                model.ListaModulos = ListaModulos;
                model.ModuloActual = ListaModulos.FirstOrDefault();
            }
            catch(Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
            return View(model);
        }

    }
}
