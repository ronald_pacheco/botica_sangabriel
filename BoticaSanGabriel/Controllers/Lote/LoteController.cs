﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BSG.DATACONTRACT;
using BSG.BUSSINESS;
using BoticaSanGabriel.ViewModels.Lote;

namespace BoticaSanGabriel.Controllers.Lote
{
    public class LoteController : Controller
    {
        //
        // GET: /Lote/

        public ActionResult Index()
        {
            bool result = false;
            string mensaje = string.Empty;
            LoteViewModel model = new LoteViewModel();
            List<LoteBE> LoteList = LoteBL.Instancia.ObtenerLotes(new LoteBE(), ref result, ref mensaje);
            model.ListaLotes = LoteList;
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Registrar(int idProducto)
        {
            bool result = false;
            string mensaje = string.Empty;
            ProductoBE Producto = new ProductoBE();
            LoteBE NuevoLote = new LoteBE();
            LoteViewModel model = new LoteViewModel();
            Producto.idProducto = idProducto;
            Producto = ProductoBL.Instancia.ObtenerProductos(Producto, ref result, ref mensaje).FirstOrDefault();
            NuevoLote.idProducto = Producto.idProducto;
            NuevoLote.nombre = Producto.nombre;
            NuevoLote.codigoProducto = Producto.codigo;
            model.Producto = Producto;
            model.Lote = NuevoLote;
            return View(model);
        }

        [HttpPost]
        public JsonResult Registrar(LoteViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            LoteBE LoteNuevo = new LoteBE();
            model.Lote.UsuarioMaster = Session["Usuario"].ToString();
            LoteNuevo = LoteBL.Instancia.RegistrarLote(model.Lote, ref result, ref mensaje);
            return Json(new { ok = result, data = LoteNuevo, mensaje = mensaje });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Editar(int idLote)
        {
            bool result = false;
            string mensaje = string.Empty;
            LoteBE EditLote = new LoteBE();
            LoteViewModel model = new LoteViewModel();
            EditLote.idLote = idLote;
            EditLote = LoteBL.Instancia.ObtenerLotes(EditLote, ref result, ref mensaje).FirstOrDefault();
            model.Lote = EditLote;
            return View(model);
        }

        [HttpPost]
        public JsonResult Editar(LoteViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            LoteBE LoteEditado = new LoteBE();
            model.Lote.UsuarioMaster = Session["Usuario"].ToString();
            LoteEditado = LoteBL.Instancia.EditarLote(model.Lote, ref result, ref mensaje);
            return Json(new { ok = result, data = LoteEditado, mensaje = mensaje });
        }

        [HttpPost]
        public JsonResult Eliminar(LoteViewModel model)
        {
            bool result = false;
            string mensaje = string.Empty;
            LoteBE LoteEliminado = new LoteBE();
            model.Lote.UsuarioMaster = Session["Usuario"].ToString();
            LoteEliminado = LoteBL.Instancia.EliminarLote(model.Lote, ref result, ref mensaje);
            return Json(new { ok = result, data = LoteEliminado, mensaje = mensaje });
        }
    }
}
